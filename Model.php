<?php
//require "config.php";
class Model{
    public $dbh;
    
    function __construct()
    {       
       //print_r($dbConfig);
       try {
           $this->dbh = new PDO('mysql:host='.DB_HOST.';dbname='.DB_DATABASE, DB_USER, DB_PASSWORD);
           
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
}