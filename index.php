<?php

include 'autoloader.php';
include 'config.php';

$requestUrl = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
$requestString = substr($requestUrl, strlen($baseUrl));
$urlParams = explode('/', $requestString);

$controllerName = ucfirst($urlParams[1]).'Controller';
$actionName = strtolower($urlParams[2]).'Action';
$controller = new $controllerName;
$controller->$actionName();