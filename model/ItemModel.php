<?php
require "/./Model.php";

class ItemModel extends Model{
    public $itemId;
    public $itemName;
    public $itemPrice;
    public $itemQty;
   
    
    public function getItemList(){
        $statement = $this->dbh->prepare("select * from item");
        $statement->execute();
        $row = $statement->fetchAll();
        return $row;
    }
    
    public function addItem(){
        $statement = $this->dbh->prepare("insert into item(name,price,stockQty) VALUES(:itemName, :itemPrice, :itemQty)");
        $statement->execute(array(
            "itemName" => $this->itemName,
            "itemPrice" => $this->itemPrice,
            "itemQty" => $this->itemQty
        ));
        return $statement->execute();
    }
    
    public function deleteItem(){
        if(!empty($this->itemId)){
            $statement = $this->dbh->prepare("delete from item where id=:itemId");
            $statement->execute(array(
                "itemId" => $this->itemId,
            ));
            return $statement->execute();
        }else{
            return false;
        }
       
    }
}