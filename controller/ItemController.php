<?php

require "/./model/ItemModel.php";

class ItemController{

    public function listAction(){
        $method = $_SERVER['REQUEST_METHOD'];
        
        if($method == "GET"){
            $itemModel = new ItemModel();
            $row = $itemModel->getItemList();
            if(count($row) > 0){
               $respond["resultCd"] = "00";
               $respond["resultMsg"] = "Success";
               $respond["resultData"] = json_encode($row);
               echo json_encode($respond);
            }else{
                $respond["resultCd"] = "01";
                $respond["resultMsg"] = "No Data Found";
                 echo json_encode($respond);
            }
            
           
        }else{
            header('HTTP/1.1 405 Method Not Allowed');
            header('Allow: GET');
        }
                
        
    }
    public function addAction(){
        $method = $_SERVER['REQUEST_METHOD'];
        
        if($method == "POST"){
            $itemName = $_POST["itemName"];
            $itemPrice = $_POST["itemName"];
            $itemQty = $_POST["itemQty"];
            
            $itemModel = new ItemModel();
            $itemModel->itemName = $itemName;
            $itemModel->itemPrice = $itemPrice;
            $itemModel->itemQty =$itemQty;
            
            $respond = array();
            if($itemModel->addItem()){
                $respond["resultCd"] = "00";
                $respond["resultMsg"] = "Success";
            }else{
                $respond["resultCd"] = "99";
                $respond["resultMsg"] = "Add Item Failed!";
            }
            echo json_encode($respond);
        }else{
            header('HTTP/1.1 405 Method Not Allowed');
            header('Allow: POST');
        }
    }
    
    public function deleteAction(){
        $method = $_SERVER['REQUEST_METHOD'];
        
        if($method == "DELETE"){
            parse_str(file_get_contents("php://input"),$params);
            $itemModel = new ItemModel();
            $itemModel->itemId = $params["itemId"];
            if($itemModel->deleteItem()){
                $respond["resultCd"] = "00";
                $respond["resultMsg"] = "Success";
            }else{
                $respond["resultCd"] = "98";
                $respond["resultMsg"] = "Delete Item Failed!";
            }
            echo json_encode($respond);
        }else{
            header('HTTP/1.1 405 Method Not Allowed');
            header('Allow: DELETE');
        }
    }
}