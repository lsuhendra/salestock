<?php

spl_autoload_register(function ($class) {
    $fileController = 'controller/' . $class . '.php';
   //$fileModel = 'model/' . $class . '.php';
    if(file_exists($fileController)){
        include $fileController;
    }
//    else if(file_exists($fileModel)){
//        include $fileModel;
//    }
    else{
        return false;
    }
    
});