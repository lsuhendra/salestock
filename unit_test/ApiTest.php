<?php
require "d:/xampp/php/pear/PHPUnit/Framework/TestCase.php";

class ApiTest extends PHPUnit_Framework_TestCase{
    
    public function testGetItem(){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://localhost/salestock/index.php/item/list");
 
        curl_setopt($ch, CURLOPT_REFERER, "http://www.example.com");        
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        
        $output = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
        curl_close($ch);
        
        $data = json_decode($output, true);
        
        $this->assertEquals(200, $httpCode);
        
        $this->assertEquals("00", $data["resultCd"]);
        
        $row = $data["resultData"];
        $this->assertGreaterThan(0, count($row["resultCd"]));
        
    }
    
}


?>